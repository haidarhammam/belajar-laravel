<!DOCTYPE html>
<html>
<head>
	<title>Form Sign Up</title>
</head>
<body>
</body>
	<h2>Buat Account Baru!</h2>
		<h3>Sign Up Form</h3>
		<form action="/newpost" method="post">
			@csrf
			<label for="fname">First name:</label><br><br>
				<input type="text" name="fname"><br><br>
			<label for="lname">Last name:</label><br><br>
				<input type="text" name="lname"><br><br>
			<label for="gender">Gender:</label><br><br>
				<input type="radio" id="male" name="gender" value="male">
			  	<label for="male">Male</label><br>
			  	<input type="radio" id="female" name="gender" value="female">
			  	<label for="female">Female</label><br>
			  	<input type="radio" id="other" name="gender" value="other">
			  	<label for="other">Other</label><br><br>
			<label for="nationally">Nationally:</label><br><br>
				<select>
				    <option value="0">Indonesian</option>
				    <option value="1">English</option>
				    <option value="2">German</option>
				    <option value="3">Dutch</option>
				    <option value="4">Japanese</option>
			  	</select><br><br>
			<label for="nationally">Language Spoken:</label><br><br>
				<input type="checkbox" id="bahasa" name="bahasa">
  				<label for="bahasa"> Bahasa Indonesia</label><br>
  				<input type="checkbox" id="english" name="english">
  				<label for="english"> English</label><br>
  				<input type="checkbox" id="other" name="other">
  				<label for="other"> Arabic</label><br>
  				<input type="checkbox" id="bahasa" name="bahasa">
  				<label for="bahasa"> Japanese</label><br><br>
  			<label for="Bio">Bio:</label><br><br>
  				<textarea></textarea><br>
  			<br>
  			<input type="submit" value="Sign Up">
		</form>
</html>